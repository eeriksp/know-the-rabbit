#!/usr/bin/env python

import os
from sys import argv
from PIL import Image

def remove_if_not_image(path: str):
	try:
		im = Image.open(path)
		print(f'Image {path} was OK')
	except IOError as e:
		if "cannot identify image file" in str(e):
			os.remove(path)
			print(f'Removed some not image pile from {path}')
		else:
			raise e

if __name__ == '__main__':
	dir_path = argv[1]
	for filename in os.listdir(dir_path):
		full_name = os.path.join(dir_path, filename)
		remove_if_not_image(full_name)